﻿using System;
using Unit4.CollectionsLib;
using System.Text.RegularExpressions;

namespace workspace
{
    class Unit4Utils
    {
        public static string ListToString<T>(Node<T> lst)
        {
            if (lst == null)
                return "null";
            string str = "";
            Node<T> pos = lst;
            while (pos != null)
            {
                str += pos.GetValue() + " → ";
                pos = pos.GetNext();
            }
            return str.Substring(0, str.Length - 1) + " null";
        }
        // public static string ListToString<T>(Node<T> lst)
        // {
        //     if (lst == null)
        //         return "null";
        //     string str = "[";
        //     Node<T> pos = lst;
        //     while (pos != null)
        //     {
        //         str += pos.GetValue() + ",";
        //         pos = pos.GetNext();
        //     }
        //     return str.Substring(0, str.Length - 1) + "]";
        // }
        public static void PrintList<T>(Node<T> lst)
        {
            Console.WriteLine(ListToString(lst));
        }
        public static Node<T> CreateList<T>(params T[] values)
        {
            Node<T> lst = null;
            for (int i = values.Length - 1; i >= 0; i--)
            {
                lst = new Node<T>(values[i], lst);
            }
            return lst;
        }
        public static Queue<T> CreateQueue<T>(params T[] values)
        {
            Queue<T> q = new Queue<T>();
            foreach (T val in values)
            {
                q.Insert(val);
            }
            return q;
        }
        public static Stack<T> CreateStack<T>(params T[] values)
        {
            Stack<T> st = new Stack<T>();
            foreach (T val in values)
            {
                st.Push(val);
            }
            return st;
        }

        public static BinNode<int> CreateBinTree(BinNode<int> root = null)
        {
            if (root == null)
            {
                Console.Write("root=");
                root = new BinNode<int>(int.Parse(Console.ReadLine()));
            }
            Console.Write($"({root}) left=");
            int left;
            if (int.TryParse(Console.ReadLine(), out left))
            {
                BinNode<int> leftNode = new BinNode<int>(left);
                root.SetLeft(leftNode);
                CreateBinTree(leftNode);
            }
            Console.Write($"({root}) right=");
            int right;
            if (int.TryParse(Console.ReadLine(), out right))
            {
                BinNode<int> rightNode = new BinNode<int>(right);
                root.SetRight(rightNode);
                CreateBinTree(rightNode);
            }
            return root;
        }
        public static string GetPreOrderFormula(BinNode<int> bt, string prefix = "Pre")
        {
            if (bt == null)
            {
                return $"{prefix}E";
            }
            if (!bt.HasLeft() && !bt.HasRight())
            {
                return $"{prefix}{bt.GetValue()}E";
            }
            string str = prefix + bt.GetValue();
            if (bt.HasLeft() != bt.HasRight())
            {
                str += "N";
            }
            if (bt.HasLeft())
            {
                str += GetPreOrderFormula(bt.GetLeft(), "L");
            }
            if (bt.HasRight())
            {
                str += GetPreOrderFormula(bt.GetRight(), "R");
            }
            return str;
        }
        public static BinNode<int> BinNodeFromBracesFormula(string formula)
        {
            if (formula.Equals("()") || formula.Equals(""))
                return null;
            if (new Regex(@"^\(\d+\)$").IsMatch(formula))
            {
                int val = int.Parse(new Regex(@"^\((?<num>\d+)\)$").Match(formula).Groups["num"].Value);
                return new BinNode<int>(val);
            }
            //(((3)2(2))1(()9(5)))
            formula = formula.Substring(1, formula.Length - 2);
            //((3)2(2))1(()9(5))
            string leftSide = ExtractLegalFormula(formula);//((3)2(2))
            //((3)2(2))1(()9(5))
            formula = formula.Substring(leftSide.Length);
            Match match = new Regex(@"^(?<value>\d+)(?<right>.*)$").Match(formula);
            int value = int.Parse(match.Groups["value"].Value);
            string rightSide = match.Groups["right"].Value;
            BinNode<int> left = BinNodeFromBracesFormula(leftSide);
            BinNode<int> right = BinNodeFromBracesFormula(rightSide);
            return new BinNode<int>(left, value, right);
        }
        private static string ExtractLegalFormula(string formula)
        {
            string str = "(";
            int k = 1, index = 1;
            while (index < formula.Length && k != 0)
            {
                if (formula[index] == '(')
                    k++;
                else if (formula[index] == ')')
                    k--;
                str += formula[index++];
            }
            return str;
        }
        public static void PrintArray<T>(T[] arr)
        {
            Console.WriteLine($"[{string.Join(',', arr)}]");
        }
    }
}